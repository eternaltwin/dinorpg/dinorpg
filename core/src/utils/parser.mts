import { ConditionEnum } from '../models/enums/Parser.mjs';
import { Condition } from '../models/npc/NpcConditions.mjs';
import { placeList } from '../models/place/PlaceList.mjs';
import { PlayerForConditionCheck } from '../constants.mjs';
import dayjs from 'dayjs';
import { ExpectedError } from '../utils/ExpectedError.mjs';
import prand from 'pure-rand';

export function conditionParser(condition: Condition, player: PlayerForConditionCheck, activeDinoz: number): boolean {
	let result;
	const GOTO = condition[ConditionEnum.GOTO];
	const PLACE_IS = condition[ConditionEnum.PLACE_IS];

	const MIN_LEVEL = condition[ConditionEnum.MINLEVEL];
	const MAX_LEVEL = condition[ConditionEnum.MAXLEVEL];
	const STATUS = condition[ConditionEnum.STATUS];
	const FINISHED_MISSION = condition[ConditionEnum.FINISHED_MISSION];
	const SKILL = condition[ConditionEnum.SKILL];
	const SCENARIO = condition[ConditionEnum.SCENARIO];
	const POSSESS_OBJECT = condition[ConditionEnum.POSSESS_OBJECT];
	const PLAYER_EPIC = condition[ConditionEnum.PLAYER_EPIC];
	const RANDOM = condition[ConditionEnum.RANDOM];
	const COLLEC = condition[ConditionEnum.COLLEC];
	const DINOZ_LIFE = condition[ConditionEnum.DINOZ_LIFE];
	const ACTIVE = condition[ConditionEnum.ACTIVE];
	const DAY = condition[ConditionEnum.DAY];
	const TIME = condition[ConditionEnum.TIME];
	const EQUIP = condition[ConditionEnum.EQUIP];
	const HOUR = condition[ConditionEnum.HOUR];

	const myDinoz = player.dinoz.find(d => d.id === activeDinoz);

	if (!myDinoz) {
		throw new ExpectedError(`No dinoz ${activeDinoz} found for parser.`);
	}

	if (MIN_LEVEL) {
		result = myDinoz.level >= MIN_LEVEL;
	} else if (MAX_LEVEL) {
		result = myDinoz.level <= MAX_LEVEL;
	} else if (STATUS) {
		result = myDinoz.status.some(st => st.statusId === STATUS);
	} else if (FINISHED_MISSION) {
		result = myDinoz.missions.find(missions => missions.missionId === FINISHED_MISSION)?.isFinished ?? false;
	} else if (SKILL) {
		result = myDinoz.skills.some(dinozSkill => dinozSkill.skillId === SKILL);
	} else if (GOTO) {
		const place = Object.entries(placeList).find(place => place[0].toUpperCase() === GOTO.toUpperCase());
		if (!place) {
			throw new Error(`Place ${GOTO} doesn't exist.`);
		}
		result = player.dinoz.every(dinoz => place[1].placeId === dinoz.placeId);
	} else if (PLACE_IS) {
		const thisplace = placeList[PLACE_IS];

		result = myDinoz.placeId === thisplace.placeId;
	} else if (SCENARIO) {
		const quest = SCENARIO[0];
		const step = SCENARIO[1];
		const target = SCENARIO[2];
		const playerQuest = player.quests.find(q => q.questId === quest);
		if (playerQuest) {
			switch (target) {
				case '=':
					result = playerQuest.progression === step;
					break;
				case '+':
					result = playerQuest.progression >= step;
					break;
				case '-':
					result = playerQuest.progression <= step;
					break;
				default:
					result = false;
					break;
			}
		} else if (step === 0 && !playerQuest) {
			result = true;
		} else {
			result = false;
		}
	} else if (POSSESS_OBJECT) {
		result =
			player.items.some(item => item.itemId === POSSESS_OBJECT) ||
			player.dinoz.some(dinoz => dinoz.items.some(item => item.itemId === POSSESS_OBJECT));
	} else if (PLAYER_EPIC) {
		result = player.rewards.some(reward => reward.rewardId === PLAYER_EPIC);
	} else if (RANDOM) {
		const score = Math.floor(Math.random() * RANDOM);
		const target = 0;
		result = score == target;
	} else if (COLLEC) {
		result = player.rewards.some(reward => reward.rewardId === COLLEC);
	} else if (DINOZ_LIFE) {
		switch (DINOZ_LIFE[0]) {
			case '==':
				return myDinoz.life === DINOZ_LIFE[1];
			case '>':
				return myDinoz.life > DINOZ_LIFE[1];
			case '>=':
				return myDinoz.life >= DINOZ_LIFE[1];
			case '<':
				return myDinoz.life < DINOZ_LIFE[1];
			case '<=':
				return myDinoz.life <= DINOZ_LIFE[1];
			default:
				return false;
		}
	} else if (ACTIVE) {
		result = ACTIVE;
	} else if (DAY !== undefined) {
		result = dayjs().day() === DAY;
	} else if (TIME) {
		const hour = dayjs().hour();
		const seed = hour + myDinoz.id;
		const rng = prand.xoroshiro128plus(seed);
		const value = prand.unsafeUniformIntDistribution(0, TIME, rng); // value between 0 and TIME (based on actual hour + dinozId)
		result = value === 0;
	} else if (EQUIP) {
		result = myDinoz.items.some(i => i.itemId === EQUIP);
	} else if (HOUR) {
		const hour = dayjs().hour();
		result = hour === HOUR;
	} else {
		result = false;
	}

	if (!result) result = false;

	return result;
}
