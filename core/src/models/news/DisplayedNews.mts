export interface DisplayedNews {
	id: number;
	createdDate: Date;
	title: string;
	text: string;
	hide: boolean;
}
