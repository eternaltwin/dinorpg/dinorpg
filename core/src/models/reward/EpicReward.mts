export interface EpicReward {
	id: number;
	name: string;
	displayed: boolean;
	announced: boolean;
}
