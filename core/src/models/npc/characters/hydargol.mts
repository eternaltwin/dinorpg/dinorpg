import { DinozStatusId } from '../../dinoz/StatusList.mjs';
import { ConditionEnum, Operator, RewardEnum } from '../../enums/Parser.mjs';
import { Reward } from '../../reward/RewardList.mjs';
import { NpcData } from '../NpcData.mjs';

export const HYDARGOL: Readonly<Record<string, NpcData>> = {
	begin: {
		stepName: 'begin',
		nextStep: ['talk'],
		initialStep: true
	},
	talk: {
		stepName: 'talk',
		nextStep: ['hello', 'help', 'give', 'act'],
		alias: 'hello'
	},
	hello: {
		stepName: 'hello',
		nextStep: [],
		target: 'talk'
	},
	help: {
		stepName: 'help',
		condition: {
			[Operator.AND]: [
				{ [Operator.NOT]: { [ConditionEnum.STATUS]: DinozStatusId.NENUPHAR_LEAF } },
				{ [Operator.NOT]: { [ConditionEnum.COLLEC]: Reward.PERLE } }
			]
		},
		nextStep: ['get']
	},
	give: {
		stepName: 'give',
		condition: {
			[Operator.AND]: [
				{ [ConditionEnum.STATUS]: DinozStatusId.NENUPHAR_LEAF },
				{ [Operator.NOT]: { [ConditionEnum.COLLEC]: Reward.PERLE } }
			]
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: DinozStatusId.NENUPHAR_LEAF,
				reverse: true
			},
			{
				rewardType: RewardEnum.EPIC,
				value: Reward.PERLE
			}
		],
		nextStep: []
	},
	act: {
		stepName: 'act',
		condition: {
			[ConditionEnum.COLLEC]: Reward.PERLE
		},
		nextStep: ['gant']
	},
	get: {
		stepName: 'get',
		nextStep: ['where']
	},
	where: {
		stepName: 'where',
		nextStep: [],
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: DinozStatusId.ZENBRO
			}
		]
	},
	gant: {
		stepName: 'gant',
		nextStep: ['why'],
		condition: {
			[Operator.AND]: [
				{ [ConditionEnum.STATUS]: DinozStatusId.ZORS_GLOVE },
				{ [Operator.NOT]: { [ConditionEnum.STATUS]: DinozStatusId.NENUPHAR_LEAF } }
			]
		}
	},
	ok: {
		stepName: 'ok',
		nextStep: [],
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: DinozStatusId.NENUPHAR_LEAF
			}
		]
	},
	why: {
		stepName: 'why',
		nextStep: ['super'],
		alias: 'no'
	},
	super: {
		stepName: 'super',
		nextStep: ['ok', 'no']
	},
	no: {
		stepName: 'no',
		target: 'why',
		nextStep: []
	},
	stop: {
		stepName: 'stop',
		nextStep: []
	}
};
