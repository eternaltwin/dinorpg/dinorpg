export enum Energy {
	E80 = 80,
	E75 = 75,
	E65 = 65,
	E60 = 60,
	E55 = 55,
	E50 = 50,
	E45 = 45,
	E40 = 40,
	E35 = 35,
	E30 = 30,
	E25 = 25,
	E20 = 20,
	E10 = 10,
	E5 = 5,
	NONE = 0
}
