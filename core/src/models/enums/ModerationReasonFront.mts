export enum ModerationReasonFront {
	MULTI = 'multi',
	ACCOUNTNAME = 'accountName',
	AVATAR = 'avatar',
	CUSTOMTEXT = 'customText',
	DINOZNAME = 'dinozName'
}
