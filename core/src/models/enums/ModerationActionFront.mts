export enum ModerationActionFront {
	CLOSED = 'closed',
	WARNING = 'warning',
	SHORT_BAN = 'shortBan',
	MEDIUM_BAN = 'mediumBan',
	LONG_BAN = 'longBan',
	INFINITE_BAN = 'infiniteBan'
}
