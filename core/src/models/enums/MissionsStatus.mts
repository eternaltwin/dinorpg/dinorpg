export enum MissionsStatus {
	UNAVAILABLE = 'unavailable',
	AVAILABLE = 'available',
	ONGOING = 'ongoing',
	FINISHED = 'finished'
}
