export interface PlayerRanking {
	dinozCount: number;
	pointCount: number;
	playerName: string;
	playerId: string;
	pointAverage: number;
	position: number;
}
