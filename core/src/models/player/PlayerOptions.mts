export interface PlayerOptions {
	hasPDA: boolean;
	hasPMI: boolean;
	currentDinozId?: number;
}
