export interface ChannelData {
	connectionId: string;
	playerId: string;
}
