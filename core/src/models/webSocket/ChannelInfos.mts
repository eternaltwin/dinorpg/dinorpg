import { ChannelData } from './ChannelData.mjs';

export interface ChannelInfos {
	channelName: string;
	members: ChannelData[];
}
