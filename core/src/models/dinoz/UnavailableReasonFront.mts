export enum UnavailableReasonFront {
	frozen = 'frozen',
	sacrificed = 'sacrificed',
	selling = 'selling',
	superdom = 'superdom',
	resting = 'resting'
}
