export interface SiteAchiev {
	name: string;
	requirement: string;
	quantity: number;
}
