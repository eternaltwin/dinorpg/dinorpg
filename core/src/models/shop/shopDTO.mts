export interface ShopDTO {
	itemId: number;
	quantity: number;
}
