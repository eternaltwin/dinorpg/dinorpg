import { Request } from 'express';
import { getAllDinozFromAccount, updateDinoz } from '../dao/dinozDao.js';
import { addMultipleSkillToDinoz, removeSkillFromDinoz } from '../dao/dinozSkillDao.js';
import { addMultipleStatusToDinoz, removeStatusFromDinoz } from '../dao/dinozStatusDao.js';
import {
	addMoney,
	auth,
	getAllInformationFromPlayer,
	getEternalTwinId,
	removeMoney,
	setPlayer
} from '../dao/playerDao.js';
import { addMultipleRewardToPlayer, removeRewardFromPlayer } from '../dao/playerRewardsDao.js';
import { addNewSecret, getAllSecretsRequest } from '../dao/secretDao.js';
import { increaseItemQuantity, decreaseItemQuantity } from '../dao/playerItemDao.js';
import { increaseIngredientQuantity, decreaseIngredientQuantity } from '../dao/playerIngredientDao.js';
import { increaseQuestProgression, decreaseQuestProgression } from '../dao/questsDao.js';
import { createLog } from '../dao/logDao.js';
import { LogType } from '@drpg/prisma';
import { AdminRole } from '@drpg/prisma';
import { ExpectedError } from '@drpg/core/utils/ExpectedError';
import { ModerationAdminType } from '@drpg/core/models/admin/ModerationType';

/**
 * @summary Check if user can access the admin dashboard
 * @param req
 * @return boolean
 */
export async function getAdminDashBoard(req: Request): Promise<boolean> {
	await auth(req);
	return true;
}

/**
 * @summary Edit most of the element from a dinoz
 * @param req
 * @param req.params.id {string} DinozId
 * @param req.body.name {string} New Dinoz name
 * @param req.body.unavailableReason {string} Dinoz is unavailable or not
 * @param req.body.unavailableReasonOperation {string} Operation done to unavailableReason (add or remove)
 * @param req.body.level {number} New Dinoz level
 * @param req.body.placeId {number} New Dinoz placeId
 * @param req.body.canChangeName {boolean} Can change its name or not
 * @param req.body.life {number} New Dinoz life
 * @param req.body.maxLife {number} New Dinoz maximum life
 * @param req.body.experience {number} New Dinoz experience
 * @param req.body.addStatus {number} Status to add to the dinoz
 * @param req.body.removeStatus {number} Status to remove to the dinoz
 * @param req.body.addSkill {number} Skill to add to the dinoz
 * @param req.body.removeSkill {number} Skill to remove to the dinoz
 */
export async function editDinoz(req: Request) {
	const authed = await auth(req);

	let unavailableReason;

	switch (req.body.unavailableReasonOperation) {
		case 'add':
			unavailableReason = req.body.unavailableReason;
			break;
		case 'remove':
			unavailableReason = null;
			break;
		case '':
		// Do nothing
	}

	const dinoz = {
		name: req.body.name,
		canChangeName: req.body.canChangeName,
		unavailableReason: unavailableReason,
		level: req.body.level,
		placeId: req.body.placeId,
		life: req.body.life,
		maxLife: req.body.maxLife,
		experience: req.body.experience
	};

	await updateDinoz(+req.params.id, dinoz);

	if (typeof dinoz.name !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'name', dinoz.name);
	}
	if (typeof dinoz.canChangeName !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'canChangeName', dinoz.canChangeName);
	}
	if (typeof dinoz.unavailableReason !== 'undefined' && dinoz.unavailableReason !== null) {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'unavailableReason', dinoz.unavailableReason);
	}
	if (typeof dinoz.level !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'level', dinoz.level);
	}
	if (typeof dinoz.placeId !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'placeId', dinoz.placeId);
	}
	if (typeof dinoz.life !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'life', dinoz.life);
	}
	if (typeof dinoz.maxLife !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'maxLife', dinoz.maxLife);
	}
	if (typeof dinoz.experience !== 'undefined') {
		await createLog(LogType.AdminUpdateDinoz, authed.id, +req.params.id, 'experience', dinoz.experience);
	}

	const statusListAsString: string[] = req.body.status;
	const statusList = statusListAsString.map(status => +status);
	if (statusList.length > 0 && req.body.statusOperation) {
		switch (req.body.statusOperation) {
			case 'add':
				await addMultipleStatusToDinoz(+req.params.id, statusList);

				for (const status of statusList) {
					await createLog(LogType.AdminAddStatus, authed.id, +req.params.id, status);
				}
				break;
			case 'remove':
				for (const status of statusList) {
					await removeStatusFromDinoz(parseInt(req.params.id), status);

					await createLog(LogType.AdminRemoveStatus, authed.id, +req.params.id, status);
				}
				break;
			default:
				throw new ExpectedError(`You need to select an operation.`);
		}
	}

	const skillList: number[] = req.body.skill;
	if (skillList.length > 0 && req.body.skillOperation) {
		switch (req.body.skillOperation) {
			case 'add':
				await addMultipleSkillToDinoz(+req.params.id, skillList);

				for (const skill of skillList) {
					await createLog(LogType.AdminAddSkill, authed.id, +req.params.id, skill);
				}
				break;
			case 'remove':
				const promises = skillList.map(skill => removeSkillFromDinoz(+req.params.id, skill));
				await Promise.all(promises);

				for (const skill of skillList) {
					await createLog(LogType.AdminRemoveSkill, authed.id, +req.params.id, skill);
				}
				break;
			default:
				throw new ExpectedError(`You need to select an operation.`);
		}
	}
}

/**
 * @summary Add or remove gold to a player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.operation {string} Operation to be realised (add or remove)
 * @param req.body.epic {number} Quantity of gold
 * @return string
 */
export async function setPlayerMoney(req: Request) {
	const authed = await auth(req);

	const player = await getEternalTwinId(req.params.id);
	if (!player) {
		throw new ExpectedError(`Player ${req.params.id} doesn't exist.`);
	}
	let newMoney = 0;
	switch (req.body.operation) {
		case 'add':
			await createLog(LogType.AdminAddMoney, authed.id, undefined, +req.params.id, req.body.gold);
			newMoney = (await addMoney(authed.id, +req.body.gold)).money;
			break;
		case 'remove':
			await createLog(LogType.AdminRemoveMoney, authed.id, undefined, +req.params.id, req.body.gold);
			newMoney = (await removeMoney(authed.id, +req.body.gold)).money;
			break;
		default:
			throw new ExpectedError(`You need to select an operation.`);
	}

	return newMoney.toString();
}

/**
 * @summary Add or remove epic reward to a player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.operation {string} Operation to be realised (add or remove)
 * @param req.body.epic {number} Id of the Epic reward
 * @return void
 */
export async function givePlayerEpicReward(req: Request): Promise<void> {
	const authed = await auth(req);

	const rewardList: number[] = req.body.epicRewardId;
	switch (req.body.operation) {
		case 'add':
			await addMultipleRewardToPlayer(
				rewardList.map(reward => ({
					playerId: req.params.id,
					rewardId: +reward
				}))
			);

			for (const reward of rewardList) {
				await createLog(LogType.AdminAddReward, authed.id, undefined, req.params.id, reward);
			}
			break;
		case 'remove':
			const promises = rewardList.map(reward => removeRewardFromPlayer(req.params.id, +reward));
			await Promise.all(promises);

			for (const reward of rewardList) {
				await createLog(LogType.AdminRemoveReward, authed.id, undefined, req.params.id, reward);
			}
			break;
		default:
			throw new ExpectedError(`You need to select an operation.`);
	}
}

/**
 * @summary Add, remove, or modify item quantities for a player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.operation {string} Operation to be realized (increase, decrease)
 * @param req.body.items {Array<{id: number, quantity: number}>} List of items and their quantities
 * @return void
 */
export async function modifyPlayerItems(req: Request): Promise<void> {
	const authed = await auth(req);

	const items: { id: number; quantity: number }[] = req.body.items;
	switch (req.body.operation) {
		case 'increase':
			for (const item of items) {
				await increaseItemQuantity(req.params.id, item.id, item.quantity);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, item.id, item.quantity);
			}
			break;
		case 'decrease':
			for (const item of items) {
				await decreaseItemQuantity(req.params.id, item.id, item.quantity);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, item.id, item.quantity);
			}
			break;
		default:
			throw new ExpectedError(`You need to select a valid operation.`);
	}
}

/**
 * @summary Add, remove, or modify ingredients quantities for a player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.operation {string} Operation to be realized (increase, decrease)
 * @param req.body.ingredients {Array<{id: number, quantity: number}>} List of ingredients and their quantities
 * @return void
 */
export async function modifyPlayerIngredients(req: Request): Promise<void> {
	const authed = await auth(req);

	const ingredients: { id: number; quantity: number }[] = req.body.ingredients;
	switch (req.body.operation) {
		case 'increase':
			for (const ing of ingredients) {
				await increaseIngredientQuantity(req.params.id, ing.id, ing.quantity);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, ing.id, ing.quantity);
			}
			break;
		case 'decrease':
			for (const ing of ingredients) {
				await decreaseIngredientQuantity(req.params.id, ing.id, ing.quantity);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, ing.id, ing.quantity);
			}
			break;
		default:
			throw new ExpectedError(`You need to select a valid operation.`);
	}
}

/**
 * @summary Update quest progression for a player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.questId {number} Quest ID to be updated
 * @param req.body.progression {number} New progression value for the quest
 * @return void
 */
export async function updatePlayerQuestProgression(req: Request): Promise<void> {
	const authed = await auth(req);

	const quests: { questId: number; progression: number }[] = req.body.quests;
	// Validate questId and progression
	if (quests === undefined) {
		throw new ExpectedError(`Quest ID and progression are required.`);
	}
	switch (req.body.operation) {
		case 'increase':
			for (const q of quests) {
				await increaseQuestProgression(req.params.id, q.questId, q.progression);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, q.questId, q.progression);
			}
			break;
		case 'decrease':
			for (const q of quests) {
				await decreaseQuestProgression(req.params.id, q.questId, q.progression);
				await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, q.questId, q.progression);
			}
			break;
		default:
			throw new ExpectedError(`You need to select a valid operation.`);
	}
}

/**
 * @summary List all dinoz from a player
 * @param req
 * @param req.params.id {string} PlayerId
 */
export async function listAllDinozFromPlayer(req: Request) {
	const dinozList = await getAllDinozFromAccount(req.params.id);
	const dinozListToSend = dinozList.map(dinoz => {
		return {
			id: dinoz.id,
			name: dinoz.name,
			unavailableReason: dinoz.unavailableReason,
			level: dinoz.level,
			canChangeName: dinoz.canChangeName,
			leaderId: dinoz.leaderId,
			life: dinoz.life,
			maxLife: dinoz.maxLife,
			experience: dinoz.experience,
			placeId: dinoz.placeId,
			status: dinoz.status.map(status => status.statusId),
			skills: dinoz.skills.map(skill => skill.skillId)
		};
	});
	return dinozListToSend;
}

/**
 * @summary Edit a selected player
 * @param req
 * @param req.params.id {number} PlayerId
 * @param req.body.hasImported {boolean}
 * @param req.body.customText {string}
 * @param req.body.quetzuBought {number}
 * @param req.body.leader {boolean}
 * @param req.body.engineer {boolean}
 * @param req.body.cooker {boolean}
 * @param req.body.shopKeeper {boolean}
 * @param req.body.merchant {boolean}
 * @param req.body.priest {boolean}
 * @param req.body.teacher {boolean}
 * @param req.body.messie {boolean}
 * @param req.body.matelasseur {boolean}
 * @param req.body.role {"admin" | "beta" | "player"}
 */
export async function editPlayer(req: Request) {
	const authed = await auth(req);

	let role;
	switch (req.body.role) {
		case 'admin':
			role = AdminRole.ADMIN;
			break;
		case 'beta':
			role = AdminRole.BETA;
			break;
		case 'player':
			role = AdminRole.PLAYER;
			break;
		default:
			role = undefined;
	}

	const player = {
		hasImported: req.body.hasImported,
		customText: req.body.customText,
		quetzuBought: req.body.quetzuBought,
		dailyGridRewards: req.body.dailyGridRewards,
		leader: req.body.leader,
		engineer: req.body.engineer,
		cooker: req.body.cooker,
		shopKeeper: req.body.shopKeeper,
		merchant: req.body.merchant,
		priest: req.body.priest,
		teacher: req.body.teacher,
		messie: req.body.messie,
		matelasseur: req.body.matelasseur,
		role: role
	};

	await setPlayer(req.params.id, player);

	if (typeof player.hasImported !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'hasImported', player.hasImported);
	}
	if (typeof player.customText !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'customText', player.customText);
	}
	if (typeof player.quetzuBought !== 'undefined') {
		await createLog(
			LogType.AdminUpdatePlayer,
			authed.id,
			undefined,
			req.params.id,
			'quetzuBought',
			player.quetzuBought
		);
	}
	if (typeof player.dailyGridRewards !== 'undefined') {
		await createLog(
			LogType.AdminUpdatePlayer,
			authed.id,
			undefined,
			req.params.id,
			'dailyGridRewards',
			player.dailyGridRewards
		);
	}
	if (typeof player.leader !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'leader', player.leader);
	}
	if (typeof player.engineer !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'engineer', player.engineer);
	}
	if (typeof player.cooker !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'cooker', player.cooker);
	}
	if (typeof player.shopKeeper !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'shopKeeper', player.shopKeeper);
	}
	if (typeof player.merchant !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'merchant', player.merchant);
	}
	if (typeof player.priest !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'priest', player.priest);
	}
	if (typeof player.teacher !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'teacher', player.teacher);
	}
	if (typeof player.messie !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'messie', player.messie);
	}
	if (typeof player.matelasseur !== 'undefined') {
		await createLog(LogType.AdminUpdatePlayer, authed.id, undefined, req.params.id, 'matelasseur', player.matelasseur);
	}
}

/**
 * @summary List all information from a player
 * @param req
 * @param req.params.id {number} PlayerId
 */
export async function listAllPlayerInformationForAdminDashboard(req: Request) {
	const player = await getAllInformationFromPlayer(req.params.id);
	if (!player) {
		throw new ExpectedError(`Player ${req.params.id} doesn't exist.`);
	}

	const playerToSend = {
		id: player.id,
		banCase: player.banCase as ModerationAdminType,
		customText: player.customText,
		name: player.name,
		money: player.money,
		quetzuBought: player.quetzuBought,
		dailyGridRewards: player.dailyGridRewards,
		leader: player.leader,
		engineer: player.engineer,
		cooker: player.cooker,
		shopKeeper: player.shopKeeper,
		merchant: player.merchant,
		priest: player.priest,
		teacher: player.teacher,
		messie: player.messie,
		matelasseur: player.matelasseur,
		createdDate: player.createdDate,
		rewards: player.rewards.map(reward => reward.rewardId),
		items: player.items.map(item => ({
			itemId: item.itemId,
			quantity: item.quantity
		})),
		ingredients: player.ingredients.map(ing => ({
			ingredientId: ing.ingredientId,
			quantity: ing.quantity
		})),
		quests: player.quests.map(q => ({
			questId: q.questId,
			progression: q.progression
		})),
		role: player.role
	};
	return playerToSend;
}

/**
 * @summary Get all secrets stored
 */
export async function getAllSecrets() {
	const secrets = await getAllSecretsRequest();
	const response = secrets.map(secret => {
		return {
			key: secret.key,
			value: secret.value
		};
	});
	return response;
}

/**
 * @summary Add a secret to the store
 */
export async function addSecret(req: Request) {
	const authed = await auth(req);

	await addNewSecret({
		key: req.body.key,
		value: req.body.value
	});
	const secrets = await getAllSecretsRequest();

	await createLog(LogType.AdminUpdateSecret, authed.id, undefined, req.body.key, req.body.value);

	return secrets;
}
