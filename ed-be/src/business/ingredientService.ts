import { ingredientList } from '@drpg/core/models/ingredient/ingredientList';
import { Request } from 'express';
import { getAllIngredientsDataRequest } from '../dao/playerIngredientDao.js';
import { ExpectedError } from '@drpg/core/utils/ExpectedError';
import { auth } from '../dao/playerDao.js';

/**
 * Get all the ingredients from a player
 * @param req
 * @returns Array<IngredientFiche>
 * 				An array with all ingredients that player owns
 */
export async function getAllIngredientsData(req: Request) {
	const authed = await auth(req);
	const allIngredientsData = await getAllIngredientsDataRequest(authed.id);

	const ingredients = allIngredientsData.map(ingr => {
		const ingredientFound = Object.entries(ingredientList).find(
			([, value]) => value.ingredientId === ingr.ingredientId
		);

		if (!ingredientFound) throw new ExpectedError('Ingredient not found');

		return {
			ingredientId: ingr.ingredientId,
			name: ingredientFound[0].toLowerCase(),
			quantity: ingr.quantity,
			maxQuantity: ingredientFound[1].maxQuantity
		};
	});

	return ingredients;
}
