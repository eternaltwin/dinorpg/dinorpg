export const pigmou: Pigmou = {
	head: {
		position: {
			0: {
				top: 0,
				left: 0
			},
			1: {
				top: -7.5,
				left: 0
			}
		},
		special: {
			top: 1,
			left: 0
		},
		injury: {
			minor: {
				0: {
					imgNumber: 0,
					top: 18,
					left: 32
				},
				1: {
					imgNumber: 1,
					top: 26,
					left: 15
				},
				2: {
					imgNumber: 2,
					top: 5,
					left: 25.5
				},
				3: {
					imgNumber: 1,
					top: 41,
					left: 34
				},
				4: {
					imgNumber: 4,
					top: 32,
					left: 50,
					rotation: 135
				},
				5: {
					imgNumber: 5,
					top: 60,
					left: 31
				}
			}
		},
		eye: {
			0: {
				eyebrow: {
					imgNumber: 0,
					top: 31,
					left: 3
				},
				sclera: {
					normal: {
						imgNumber: 0,
						top: 42,
						left: 5
					},
					demon: {
						imgNumber: 0,
						top: 42,
						left: 5
					}
				},
				pupil: {
					dependentImage0: {
						demon: {
							imgNumber: 0,
							top: 45.5,
							left: 8
						}
					}
				},
				injury: {
					heavy: {
						0: {
							imgNumber: 0,
							top: 43.8,
							left: 23.1
						},
						1: {
							imgNumber: 1,
							top: 0.5,
							left: 20
						},
						2: {
							imgNumber: 2,
							top: 2,
							left: 43,
							zIndex: 5
						},
						3: {
							imgNumber: 3,
							top: 18.5,
							left: 17
						},
						4: {
							imgNumber: 4,
							top: 43.5,
							left: 26,
							rotation: 90
						},
						5: {
							imgNumber: 5,
							top: 57,
							left: 31
						},
						6: {
							imgNumber: 12,
							top: 34,
							left: 39.3
						}
					}
				}
			},
			1: {
				eyebrow: {
					imgNumber: 1,
					top: 25,
					left: 2
				},
				sclera: {
					normal: {
						imgNumber: 1,
						top: 43.5,
						left: 4
					},
					demon: {
						imgNumber: 1,
						top: 43.5,
						left: 4
					}
				},
				injury: {
					heavy: {
						0: {
							imgNumber: 6,
							top: 46,
							left: 14.5
						},
						1: {
							imgNumber: 1,
							top: 0.5,
							left: 20
						},
						2: {
							imgNumber: 2,
							top: 2,
							left: 43,
							zIndex: 0
						},
						3: {
							imgNumber: 3,
							top: 18.5,
							left: 17
						},
						4: {
							imgNumber: 4,
							top: 45,
							left: 23,
							rotation: 90
						},
						5: {
							imgNumber: 5,
							top: 57,
							left: 30
						},
						6: {
							imgNumber: 7,
							top: 32.5,
							left: 35.5
						}
					}
				}
			},
			2: {
				eyebrow: {
					imgNumber: 2,
					top: 27,
					left: 3.5
				},
				sclera: {
					normal: {
						imgNumber: 2,
						top: 38.4,
						left: 6.5
					},
					demon: {
						imgNumber: 2,
						top: 38.4,
						left: 6.5
					}
				},
				pupil: {
					imgNumber: 0,
					baby: {
						top: 40,
						left: 6.5
					},
					adult: {
						top: 41.5,
						left: 8
					},
					dependentImage0: {
						normal: {
							imgNumber: 0,
							top: 40.5,
							left: 6.5
						}
					}
				},
				injury: {
					heavy: {
						0: {
							imgNumber: 8,
							top: 45.5,
							left: 20.5
						},
						1: {
							imgNumber: 1,
							top: 0.5,
							left: 20
						},
						2: {
							imgNumber: 2,
							top: 2,
							left: 43,
							zIndex: 0
						},
						3: {
							imgNumber: 3,
							top: 18.5,
							left: 17
						},
						4: {
							imgNumber: 4,
							top: 45,
							left: 22,
							rotation: 120
						},
						5: {
							imgNumber: 5,
							top: 57.5,
							left: 31
						},
						6: {
							imgNumber: 9,
							top: 39,
							left: 35
						}
					}
				}
			},
			3: {
				eyebrow: {
					imgNumber: 3,
					top: 24.5,
					left: 2
				},
				sclera: {
					normal: {
						imgNumber: 3,
						top: 39,
						left: 4.2
					},
					demon: {
						imgNumber: 3,
						top: 39,
						left: 4.2
					}
				},
				pupil: {
					imgNumber: 1,
					baby: {
						top: 39.5,
						left: 5
					},
					adult: {
						top: 41,
						left: 6
					},
					dependentImage0: {
						normal: {
							imgNumber: 1,
							top: 41,
							left: 4
						}
					}
				},
				injury: {
					heavy: {
						0: {
							imgNumber: 10,
							top: 44,
							left: 20
						},
						1: {
							imgNumber: 1,
							top: 0.5,
							left: 20
						},
						2: {
							imgNumber: 2,
							top: 2,
							left: 43,
							zIndex: 0
						},
						3: {
							imgNumber: 3,
							top: 18.5,
							left: 17
						},
						4: {
							imgNumber: 4,
							top: 43.5,
							left: 24,
							rotation: 90
						},
						5: {
							imgNumber: 5,
							top: 57.5,
							left: 31
						},
						6: {
							imgNumber: 11,
							top: 32,
							left: 36
						}
					}
				}
			}
		},
		mouth: {
			0: {
				imgNumber: 0,
				top: 54.5,
				left: 8
			},
			1: {
				imgNumber: 0,
				top: 53.5,
				left: 8,
				dependentImage0: {
					normal: {
						imgNumber: 1,
						imgZIndex: 5,
						top: 58,
						left: 3
					}
				}
			},
			2: {
				imgNumber: 2,
				top: 49,
				left: 7,
				dependentImage0: {
					normal: {
						imgNumber: 3,
						imgZIndex: 4,
						top: 52.5,
						left: 8.5
					}
				}
			},
			3: {
				imgNumber: 2,
				top: 49,
				left: 7,
				dependentImage0: {
					normal: {
						imgNumber: 1,
						imgZIndex: 5,
						top: 61,
						left: 2
					}
				},
				dependentImage1: {
					imgNumber: 3,
					top: 52.5,
					left: 8.5
				}
			},
			4: {
				imgNumber: 4,
				top: 52.5,
				left: 5,
				dependentImage0: {
					normal: {
						imgNumber: 5,
						imgZIndex: 5,
						top: 57,
						left: 5.5
					},
					demon: {
						imgNumber: 8,
						imgZIndex: 5,
						top: 56.5,
						left: 4
					}
				}
			},
			5: {
				imgNumber: 4,
				top: 52.5,
				left: 5,
				dependentImage0: {
					normal: {
						imgNumber: 5,
						imgZIndex: 5,
						top: 57,
						left: 5.5
					},
					demon: {
						imgNumber: 8,
						imgZIndex: 7,
						top: 56.5,
						left: 4
					}
				},
				dependentImage1: {
					imgNumber: 1,
					top: 60,
					left: 1
				}
			},
			6: {
				imgNumber: 6,
				top: 53,
				left: 8
			},
			7: {
				imgNumber: 6,
				top: 53,
				left: 8,
				dependentImage0: {
					normal: {
						imgNumber: 1,
						imgSize: 0.7,
						imgZIndex: 5,
						top: 60,
						left: 5
					}
				}
			},
			8: {
				imgNumber: 7,
				top: 52,
				left: 8
			},
			9: {
				imgNumber: 7,
				top: 52,
				left: 8,
				dependentImage0: {
					normal: {
						imgNumber: 1,
						imgSize: 0.7,
						imgZIndex: 0,
						top: 62,
						left: 4
					}
				}
			}
		}
	},
	body: {
		default: {
			top: 61,
			left: 31
		},
		leg: {
			front: {
				left: {
					baby: {
						imgSize: 0.8,
						position: {
							top: 3,
							left: -5
						},
						nail: {
							top: 16.7,
							left: 14
						},
						fur: {
							top: 0,
							left: 7
						},
						special: {
							top: 3.5,
							left: 1.8
						}
					},
					adult: {
						imgSize: 1,
						position: {
							top: 0,
							left: 0
						},
						nail: {
							top: 17,
							left: 24
						},
						fur: {
							top: -4,
							left: 13.5
						},
						special: {
							top: 0.5,
							left: 4.5
						}
					},
					demon: {
						imgSize: 1.2,
						position: {
							top: -7,
							left: 2
						},
						nail: {
							top: 14,
							left: 30
						},
						fur: {
							top: -10,
							left: 16
						},
						special: {
							top: -7,
							left: 7
						}
					}
				},
				right: {
					baby: {
						top: -10,
						left: -36,
						imgSize: 0.8,
						nail: {
							top: 21.5,
							left: -26
						},
						fur: {
							top: -14,
							left: -43
						},
						injury: {
							minor: {
								top: 2.5,
								left: -35
							},
							heavy: {
								0: {
									top: 2.5,
									left: -35
								},
								1: {
									top: 2.8,
									left: -31.5
								}
							}
						},
						special: {
							top: -7,
							left: -29.5
						}
					},
					adult: {
						top: -15,
						left: -42,
						imgSize: 1,
						nail: {
							top: 24,
							left: -29
						},
						fur: {
							top: -19,
							left: -47
						},
						injury: {
							minor: {
								top: 2.5,
								left: -35
							},
							heavy: {
								0: {
									top: 2.5,
									left: -35
								},
								1: {
									top: 2.8,
									left: -31.5
								}
							}
						},
						special: {
							top: -14,
							left: -41
						}
					},
					demon: {
						top: -21,
						left: -45,
						imgSize: 1.1,
						nail: {
							top: 22,
							left: -31
						},
						fur: {
							top: -25,
							left: -50
						},
						injury: {
							minor: {
								top: 2.5,
								left: -35
							},
							heavy: {
								0: {
									top: 0,
									left: -36
								},
								1: {
									top: 0,
									left: -33
								}
							}
						},
						special: {
							top: -20,
							left: -44
						}
					}
				}
			},
			back: {
				right: {
					baby: {
						top: -15,
						left: -49,
						imgSize: 0.9,
						nail: {
							top: 8.5,
							left: -46
						},
						fur: {
							top: -16,
							left: -54
						},
						special: {
							top: -9.5,
							left: -37
						}
					},
					adult: {
						top: -16,
						left: -53,
						imgSize: 1,
						nail: {
							top: 10,
							left: -50
						},
						fur: {
							top: -17,
							left: -58
						},
						special: {
							top: -12,
							left: -51
						}
					},
					demon: {
						top: -28,
						left: -62,
						imgSize: 1.35,
						nail: {
							top: 7.5,
							left: -58
						},
						fur: {
							top: -29,
							left: -70
						},
						special: {
							top: -23,
							left: -59
						}
					}
				}
			}
		},
		chest: {
			baby: {
				imgSize: 0.9,
				top: -20.5,
				left: -44,
				special: {
					top: -15,
					left: -33
				}
			},
			adult: {
				imgSize: 1,
				top: -24,
				left: -46,
				special: {
					top: -22,
					left: -46
				}
			},
			demon: {
				imgSize: 1.15,
				top: -33,
				left: -49,
				special: {
					top: -31,
					left: -50
				}
			}
		}
	},
	tail: {
		defaultPosition: {
			top: 29,
			left: -16
		},
		tail: {
			0: {
				imgNumber: 0,
				baby: {
					top: 5,
					left: 4.5
				},
				adult: {
					top: -1,
					left: -0.5
				},
				demon: {
					top: -15,
					left: -4
				}
			},
			1: {
				imgNumber: 1,
				baby: {
					top: -3,
					left: 3.5
				},
				adult: {
					top: -18.5,
					left: -3
				},
				demon: {
					top: -33,
					left: -8
				},
				dependentImage0: {
					imgNumber: 2,
					baby: {
						top: -7.5,
						left: -3
					},
					adult: {
						top: -29,
						left: -17.5
					},
					demon: {
						top: -45.5,
						left: -25.5
					}
				}
			},
			2: {
				imgNumber: 3,
				baby: {
					top: -2.5,
					left: 2,
					injury: {
						heavy: {
							top: 4.5,
							left: 5,
							size: 0.8
						}
					}
				},
				adult: {
					top: -14,
					left: -6,
					injury: {
						heavy: {
							top: 2.5,
							left: 1.5,
							size: 1.2
						}
					}
				},
				demon: {
					top: -30,
					left: -10,
					injury: {
						heavy: {
							top: -10,
							left: -1,
							size: 1.4
						}
					}
				}
			},
			3: {
				imgNumber: 4,
				baby: {
					top: -7,
					left: -1,
					injury: {
						heavy: {
							top: 2.5,
							left: 3.5,
							size: 1
						}
					}
				},
				adult: {
					top: -25,
					left: -13,
					injury: {
						heavy: {
							top: -3,
							left: -3,
							size: 1.9
						}
					}
				},
				demon: {
					top: -41,
					left: -20,
					injury: {
						heavy: {
							top: -14,
							left: -7,
							size: 2.0
						}
					}
				}
			}
		}
	},
	hair: {
		0: {
			image1: {
				imgNumber: 0,
				top: 8,
				left: 18.5,
				zIndex: 4
			}
		},
		1: {
			image1: {
				imgNumber: 1,
				top: -1,
				left: 10.5,
				zIndex: 4
			},
			image2: {
				baby: {
					imgNumber: 16,
					top: 6,
					left: 17,
					zIndex: 0
				},
				adult: {
					imgNumber: 2,
					top: 4,
					left: 13,
					zIndex: 0
				},
				demon: {
					imgNumber: 2,
					top: 4,
					left: 13,
					zIndex: 0
				}
			},
			image3: {
				adult: {
					imgNumber: 3,
					top: 5.5,
					left: 16
				},
				demon: {
					imgNumber: 19,
					top: -17.5,
					left: 10.5
				}
			}
		},
		2: {
			image1: {
				baby: {
					imgNumber: 16,
					top: 6,
					left: 17,
					zIndex: 4
				},
				adult: {
					imgNumber: 2,
					top: 4,
					left: 13
				},
				demon: {
					imgNumber: 2,
					top: 4,
					left: 13
				}
			},
			image2: {
				adult: {
					imgNumber: 3,
					top: 5.5,
					left: 16,
					zIndex: 0
				},
				demon: {
					imgNumber: 19,
					top: -17.5,
					left: 10.5,
					zIndex: 0
				}
			}
		},
		3: {
			image1: {
				imgNumber: 4,
				top: -33,
				left: -7,
				zIndex: 4
			}
		},
		4: {
			image1: {
				baby: {
					imgNumber: 24,
					top: 0,
					left: 11.3
				},
				adult: {
					imgNumber: 1,
					top: -1,
					left: 10.5
				},
				demon: {
					imgNumber: 20,
					top: -11,
					left: 4
				}
			}
		},
		5: {
			image1: {
				baby: {
					imgNumber: 5,
					top: 14,
					left: 45,
					zIndex: 4
				},
				adult: {
					imgNumber: 5,
					top: 14,
					left: 45,
					zIndex: 4
				},
				demon: {
					imgNumber: 22,
					top: 14,
					left: 45,
					zIndex: 4
				}
			},
			image2: {
				baby: {
					imgNumber: 6,
					top: 7,
					left: 4,
					zIndex: -2
				},
				adult: {
					imgNumber: 6,
					top: 7,
					left: 4,
					zIndex: -2
				},
				demon: {
					imgNumber: 21,
					top: 7,
					left: 4,
					zIndex: -2
				}
			},
			image3: {
				imgNumber: 7,
				top: 8,
				left: 18.5
			}
		},
		6: {
			image1: {
				baby: {
					imgNumber: 5,
					top: 14,
					left: 45,
					zIndex: 4
				},
				adult: {
					imgNumber: 5,
					top: 14,
					left: 45,
					zIndex: 4
				},
				demon: {
					imgNumber: 22,
					top: 14,
					left: 45,
					zIndex: 4
				}
			},
			image2: {
				baby: {
					imgNumber: 6,
					top: 7,
					left: 4,
					zIndex: -2
				},
				adult: {
					imgNumber: 6,
					top: 7,
					left: 4,
					zIndex: -2
				},
				demon: {
					imgNumber: 21,
					top: 7,
					left: 4,
					zIndex: -2
				}
			},
			image3: {
				baby: {
					imgNumber: 17,
					top: 12,
					left: 15
				},
				adult: {
					imgNumber: 8,
					top: 10,
					left: 15
				},
				demon: {
					imgNumber: 8,
					top: 10,
					left: 15
				}
			},
			image4: {
				adult: {
					imgNumber: 9,
					top: 9,
					left: 18
				},
				demon: {
					imgNumber: 23,
					top: -12,
					left: 15.5
				}
			}
		},
		7: {
			image1: {
				imgNumber: 10,
				top: 1.5,
				left: 14.5,
				zIndex: 0
			},
			image2: {
				imgNumber: 0,
				top: 8,
				left: 18.5,
				zIndex: 2
			}
		},
		8: {
			image1: {
				imgNumber: 11,
				top: 0.5,
				left: 13,
				zIndex: 0
			},
			image2: {
				imgNumber: 0,
				top: 8,
				left: 18.5,
				zIndex: 2
			}
		},
		9: {
			image1: {
				imgNumber: 12,
				top: 41,
				left: 42.5,
				zIndex: 4
			},
			image2: {
				imgNumber: 13,
				top: 26.5,
				left: -2,
				zIndex: 0
			},
			image3: {
				imgNumber: 0,
				top: 8,
				left: 18.5
			}
		},
		A: {
			image1: {
				imgNumber: 12,
				top: 41,
				left: 42.5,
				zIndex: 4
			},
			image2: {
				imgNumber: 13,
				top: 26.5,
				left: -2,
				zIndex: 0
			},
			image3: {
				baby: {
					imgNumber: 16,
					top: 6,
					left: 17
				},
				adult: {
					imgNumber: 2,
					top: 4,
					left: 13
				},
				demon: {
					imgNumber: 2,
					top: 4,
					left: 13
				}
			},
			image4: {
				adult: {
					imgNumber: 3,
					top: 5.5,
					left: 16
				},
				demon: {
					imgNumber: 19,
					top: -17.5,
					left: 10.5
				}
			}
		},
		B: {
			image1: {
				imgNumber: 12,
				top: 41,
				left: 42.5,
				zIndex: 4
			},
			image2: {
				imgNumber: 13,
				top: 26.5,
				left: -2,
				zIndex: 0
			},
			image3: {
				baby: {
					imgNumber: 1,
					top: -1,
					left: 10.5
				},
				adult: {
					imgNumber: 1,
					top: -1,
					left: 10.5
				},
				demon: {
					imgNumber: 20,
					top: -11,
					left: 4
				}
			}
		},
		C: {
			image1: {
				imgNumber: 18,
				top: 4,
				left: 22,
				zIndex: 4
			}
		},
		D: {
			image1: {
				imgNumber: 15,
				top: 30,
				left: 39,
				zIndex: 4
			},
			image2: {
				imgNumber: 14,
				top: 4,
				left: 2,
				zIndex: -2
			},
			image3: {
				imgNumber: 0,
				top: 8,
				left: 18.5
			}
		},
		E: {
			image1: {
				imgNumber: 15,
				top: 30,
				left: 39,
				zIndex: 4
			},
			image2: {
				imgNumber: 14,
				top: 4,
				left: 2,
				zIndex: -2
			},
			image3: {
				baby: {
					imgNumber: 1,
					top: -1,
					left: 10.5
				},
				adult: {
					imgNumber: 1,
					top: -1,
					left: 10.5
				},
				demon: {
					imgNumber: 20,
					top: -11,
					left: 4
				}
			}
		},
		F: {
			image1: {
				imgNumber: 15,
				top: 30,
				left: 39,
				zIndex: 4
			},
			image2: {
				imgNumber: 14,
				top: 4,
				left: 2,
				zIndex: -2
			},
			image3: {
				baby: {
					imgNumber: 16,
					top: 6,
					left: 17
				},
				adult: {
					imgNumber: 2,
					top: 4,
					left: 13
				},
				demon: {
					imgNumber: 2,
					top: 4,
					left: 13
				}
			},
			image4: {
				adult: {
					imgNumber: 3,
					top: 5.5,
					left: 16
				},
				demon: {
					imgNumber: 19,
					top: -17.5,
					left: 10.5
				}
			}
		}
	},
	color: {
		body: {
			0: {
				normal: {
					baby: {
						mainFirstColor: '#fff0a7',
						mainSecondColor: '#e5b86f',
						mainThirdColor: '#d0944b',
						mainFourthColor: '#913604',
						mainFifthColor: '#a65108',
						brighterSpecialColor: '#d4802e',
						darkerSpecialColor: '#a14309'
					},
					adult: {
						mainFirstColor: '#f0dc97',
						mainSecondColor: '#d2a863',
						mainThirdColor: '#be8540',
						mainFourthColor: '#8c3821',
						mainFifthColor: '#954601',
						brighterSpecialColor: '#c17225',
						darkerSpecialColor: '#913901'
					}
				},
				demon: {
					mainFirstColor: '#e2cb7c',
					mainSecondColor: '#c08f40',
					mainThirdColor: '#a9681a',
					mainFourthColor: '#7b2000',
					mainFifthColor: '#832e00',
					brighterSpecialColor: '#ad5200',
					darkerSpecialColor: '#761100'
				},
				special: {
					normal: {
						baby: {
							mainFirstColor: '#b8ecee',
							mainSecondColor: '#98b6b8',
							mainThirdColor: '#839294',
							mainFourthColor: '#5a4040',
							mainFifthColor: '#584f51',
							brighterSpecialColor: '#627b7f',
							darkerSpecialColor: '#414143'
						},
						adult: {
							mainFirstColor: '#a8d9db',
							mainSecondColor: '#89a5a7',
							mainThirdColor: '#758385',
							mainFourthColor: '#4e3f41',
							mainFifthColor: '#4d4446',
							brighterSpecialColor: '#566d70',
							darkerSpecialColor: '#373739'
						}
					},
					demon: {
						mainFirstColor: '#90c8ca',
						mainSecondColor: '#6d8c8f',
						mainThirdColor: '#566668',
						mainFourthColor: '#281e20',
						mainFifthColor: '#281e20',
						brighterSpecialColor: '#344f52',
						darkerSpecialColor: '#130e10'
					}
				}
			},
			1: {
				normal: {
					baby: {
						mainFirstColor: '#ffdf84',
						mainSecondColor: '#f6a74c',
						mainThirdColor: '#e18328',
						mainFourthColor: '#91311e',
						mainFifthColor: '#b64004',
						brighterSpecialColor: '#f26914',
						darkerSpecialColor: '#bb3204'
					},
					adult: {
						mainFirstColor: '#ffcb76',
						mainSecondColor: '#e29742',
						mainThirdColor: '#ce7520',
						mainFourthColor: '#861a00',
						mainFifthColor: '#a53600',
						brighterSpecialColor: '#da5b0c',
						darkerSpecialColor: '#a92900'
					}
				},
				demon: {
					mainFirstColor: '#f4b856',
					mainSecondColor: '#d27d1b',
					mainThirdColor: '#bb5600',
					mainFourthColor: '#8c0e00',
					mainFifthColor: '#901500',
					brighterSpecialColor: '#cd3a00',
					darkerSpecialColor: '#920000'
				},
				special: {
					normal: {
						baby: {
							mainFirstColor: '#ffd701',
							mainSecondColor: '#f69e02',
							mainThirdColor: '#e17a03',
							mainFourthColor: '#972202',
							mainFifthColor: '#b63804',
							brighterSpecialColor: '#f25f03',
							darkerSpecialColor: '#bb2b04'
						},
						adult: {
							mainFirstColor: '#ffc400',
							mainSecondColor: '#e28f00',
							mainThirdColor: '#ce6d00',
							mainFourthColor: '#952200',
							mainFifthColor: '#a52e00',
							brighterSpecialColor: '#de5300',
							darkerSpecialColor: '#aa2200'
						}
					},
					demon: {
						mainFirstColor: '#f5b000',
						mainSecondColor: '#d27300',
						mainThirdColor: '#bb4d00',
						mainFourthColor: '#8d0500',
						mainFifthColor: '#8d0500',
						brighterSpecialColor: '#cd2f00',
						darkerSpecialColor: '#930000'
					}
				}
			},
			2: {
				normal: {
					baby: {
						mainFirstColor: '#ffba22',
						mainSecondColor: '#f68302',
						mainThirdColor: '#e26103',
						mainFourthColor: '#971002',
						mainFifthColor: '#b71b05',
						brighterSpecialColor: '#f24004',
						darkerSpecialColor: '#b91504'
					},
					adult: {
						mainFirstColor: '#ffa919',
						mainSecondColor: '#e27500',
						mainThirdColor: '#ce5300',
						mainFourthColor: '#870900',
						mainFifthColor: '#a61300',
						brighterSpecialColor: '#de3600',
						darkerSpecialColor: '#aa0c00'
					}
				},
				demon: {
					mainFirstColor: '#f59100',
					mainSecondColor: '#d35600',
					mainThirdColor: '#bc2f00',
					mainFourthColor: '#790000',
					mainFifthColor: '#8d0000',
					brighterSpecialColor: '#ce0e00',
					darkerSpecialColor: '#920000'
				}
			},
			3: {
				normal: {
					baby: {
						mainFirstColor: '#f3873a',
						mainSecondColor: '#d45407',
						mainThirdColor: '#c02f04',
						mainFourthColor: '#8d0404',
						mainFifthColor: '#940505',
						brighterSpecialColor: '#b71905',
						darkerSpecialColor: '#870606'
					},
					adult: {
						mainFirstColor: '#e17d34',
						mainSecondColor: '#c24900',
						mainThirdColor: '#af2700',
						mainFourthColor: '#830000',
						mainFifthColor: '#860000',
						brighterSpecialColor: '#a61100',
						darkerSpecialColor: '#790000'
					}
				},
				demon: {
					mainFirstColor: '#d15f0c',
					mainSecondColor: '#ae2400',
					mainThirdColor: '#9a0100',
					mainFourthColor: '#680000',
					mainFifthColor: '#6b0000',
					brighterSpecialColor: '#8d0000',
					darkerSpecialColor: '#5a0000'
				}
			},
			4: {
				normal: {
					baby: {
						mainFirstColor: '#e3ffa9',
						mainSecondColor: '#c2d271',
						mainThirdColor: '#aeae4d',
						mainFourthColor: '#7b4f06',
						mainFifthColor: '#836b0a',
						brighterSpecialColor: '#9ca72f',
						darkerSpecialColor: '#716209'
					},
					adult: {
						mainFirstColor: '#d0f499',
						mainSecondColor: '#b2c065',
						mainThirdColor: '#9e9e42',
						mainFourthColor: '#714601',
						mainFifthColor: '#755f03',
						brighterSpecialColor: '#8d9726',
						darkerSpecialColor: '#645602'
					}
				},
				demon: {
					mainFirstColor: '#bee67e',
					mainSecondColor: '#9bab43',
					mainThirdColor: '#84841c',
					mainFourthColor: '#573d00',
					mainFifthColor: '#5a4300',
					brighterSpecialColor: '#717c00',
					darkerSpecialColor: '#423200'
				}
			},
			5: {
				normal: {
					baby: {
						mainFirstColor: '#ccef7c',
						mainSecondColor: '#acb844',
						mainThirdColor: '#979420',
						mainFourthColor: '#6b4604',
						mainFifthColor: '#6b5105',
						brighterSpecialColor: '#7b800f',
						darkerSpecialColor: '#554305'
					},
					adult: {
						mainFirstColor: '#badb6e',
						mainSecondColor: '#9ca73a',
						mainThirdColor: '#888518',
						mainFourthColor: '#613400',
						mainFifthColor: '#5f4600',
						brighterSpecialColor: '#6e7208',
						darkerSpecialColor: '#4a3900'
					}
				},
				demon: {
					mainFirstColor: '#a4ca4e',
					mainSecondColor: '#828f13',
					mainThirdColor: '#6b6800',
					mainFourthColor: '#3e2000',
					mainFifthColor: '#452e00',
					brighterSpecialColor: '#4e5200',
					darkerSpecialColor: '#291000'
				}
			},
			6: {
				normal: {
					baby: {
						mainFirstColor: '#a5deff',
						mainSecondColor: '#85a7e0',
						mainThirdColor: '#7083bb',
						mainFourthColor: '#493669',
						mainFifthColor: '#454078',
						brighterSpecialColor: '#4e69bc',
						darkerSpecialColor: '#2f3273'
					},
					adult: {
						mainFirstColor: '#96cbff',
						mainSecondColor: '#7797cd',
						mainThirdColor: '#6375aa',
						mainFourthColor: '#3d3264',
						mainFifthColor: '#3b356b',
						brighterSpecialColor: '#435dab',
						darkerSpecialColor: '#272965'
					}
				},
				demon: {
					mainFirstColor: '#7ab8f5',
					mainSecondColor: '#587cba',
					mainThirdColor: '#415692',
					mainFourthColor: '#140e4a',
					mainFifthColor: '#1b1b58',
					brighterSpecialColor: '#1a3890',
					darkerSpecialColor: '#000044'
				}
			},
			7: {
				normal: {
					baby: {
						mainFirstColor: '#99b4ec',
						mainSecondColor: '#797cb5',
						mainThirdColor: '#645890',
						mainFourthColor: '#3f1243',
						mainFifthColor: '#39154e',
						brighterSpecialColor: '#423a7a',
						darkerSpecialColor: '#271040'
					},
					adult: {
						mainFirstColor: '#8aa3d8',
						mainSecondColor: '#6b6fa4',
						mainThirdColor: '#584c82',
						mainFourthColor: '#330c3e',
						mainFifthColor: '#2f0d43',
						brighterSpecialColor: '#38306d',
						darkerSpecialColor: '#1e0936'
					}
				},
				demon: {
					mainFirstColor: '#6e8ac7',
					mainSecondColor: '#4b4f8c',
					mainThirdColor: '#352864',
					mainFourthColor: '#06001b',
					mainFifthColor: '#06001b',
					brighterSpecialColor: '#10084d',
					darkerSpecialColor: '#00000d'
				}
			},
			8: {
				normal: {
					baby: {
						mainFirstColor: '#f2873a',
						mainSecondColor: '#d45407',
						mainThirdColor: '#c02f04',
						mainFourthColor: '#880404',
						mainFifthColor: '#940505',
						brighterSpecialColor: '#b71905',
						darkerSpecialColor: '#870606'
					},
					adult: {
						mainFirstColor: '#e07c33',
						mainSecondColor: '#c24900',
						mainThirdColor: '#af2700',
						mainFourthColor: '#830000',
						mainFifthColor: '#860000',
						brighterSpecialColor: '#a61100',
						darkerSpecialColor: '#790000'
					}
				},
				demon: {
					mainFirstColor: '#d15f0c',
					mainSecondColor: '#ae2400',
					mainThirdColor: '#970000',
					mainFourthColor: '#670000',
					mainFifthColor: '#700000',
					brighterSpecialColor: '#8d0000',
					darkerSpecialColor: '#5a0000'
				}
			},
			9: {
				normal: {
					baby: {
						mainFirstColor: '#c56018',
						mainSecondColor: '#ab3304',
						mainThirdColor: '#960e05',
						mainFourthColor: '#690505',
						mainFifthColor: '#6a0606',
						brighterSpecialColor: '#790906',
						darkerSpecialColor: '#530606'
					},
					adult: {
						mainFirstColor: '#ba5e1a',
						mainSecondColor: '#9b2a00',
						mainThirdColor: '#870800',
						mainFourthColor: '#5e0000',
						mainFifthColor: '#5e0000',
						brighterSpecialColor: '#6c0200',
						darkerSpecialColor: '#480000'
					}
				},
				demon: {
					mainFirstColor: '#a43c00',
					mainSecondColor: '#810000',
					mainThirdColor: '#6a0000',
					mainFourthColor: '#3a0000',
					mainFifthColor: '#3a0000',
					brighterSpecialColor: '#4a0000',
					darkerSpecialColor: '#220000'
				}
			},
			A: {
				normal: {
					baby: {
						mainFirstColor: '#e16362',
						mainSecondColor: '#c12b2b',
						mainThirdColor: '#ac0707',
						mainFourthColor: '#7d0505',
						mainFifthColor: '#7f0606',
						brighterSpecialColor: '#980505',
						darkerSpecialColor: '#700505'
					},
					adult: {
						mainFirstColor: '#ce5757',
						mainSecondColor: '#b02222',
						mainThirdColor: '#9c0000',
						mainFourthColor: '#710000',
						mainFifthColor: '#720000',
						brighterSpecialColor: '#890000',
						darkerSpecialColor: '#600000'
					}
				},
				demon: {
					mainFirstColor: '#bc3434',
					mainSecondColor: '#980000',
					mainThirdColor: '#820000',
					mainFourthColor: '#5d0000',
					mainFifthColor: '#520000',
					brighterSpecialColor: '#6a0000',
					darkerSpecialColor: '#3c0000'
				}
			},
			B: {
				normal: {
					baby: {
						mainFirstColor: '#ffffbd',
						mainSecondColor: '#f5d885',
						mainThirdColor: '#e0b461',
						mainFourthColor: '#964512',
						mainFifthColor: '#b5701e',
						brighterSpecialColor: '#f0ac40',
						darkerSpecialColor: '#b96716'
					},
					adult: {
						mainFirstColor: '#fff9ac',
						mainSecondColor: '#e0c577',
						mainThirdColor: '#cda355',
						mainFourthColor: '#944a0f',
						mainFifthColor: '#a46316',
						brighterSpecialColor: '#dc9e38',
						darkerSpecialColor: '#a85b0e'
					}
				},
				demon: {
					mainFirstColor: '#f3ec94',
					mainSecondColor: '#d1b158',
					mainThirdColor: '#ba8a32',
					mainFourthColor: '#792000',
					mainFifthColor: '#934f00',
					brighterSpecialColor: '#ca8210',
					darkerSpecialColor: '#913800'
				}
			}
		},
		tail: {
			0: {
				normal: {
					baby: {
						tailFirstColor: '#f3ceb8',
						tailSecondColor: '#fffbe7',
						tailBorderColor: '#9e4e41',
						tailThirdColor: '#f9d6b8',
						tailFourthColor: '#e1ad99'
					},
					adult: {
						tailFirstColor: '#d8af9c',
						tailSecondColor: '#f8e5d2',
						tailBorderColor: '#954437',
						tailThirdColor: '#e3c2a7',
						tailFourthColor: '#ca9f51'
					}
				},
				demon: {
					tailFirstColor: '#d1a892',
					tailSecondColor: '#f3e3ce',
					tailBorderColor: '#8d3a24',
					tailThirdColor: '#d5b18d',
					tailFourthColor: '#ba816b'
				},
				special: {
					normal: {
						baby: {
							tailFirstColor: '#5f7d80',
							tailSecondColor: '#7fb4b6',
							tailBorderColor: '#281d1e',
							tailThirdColor: '#6a460e',
							tailFourthColor: '#4d5e60'
						},
						adult: {
							tailFirstColor: '#537072',
							tailSecondColor: '#70a1a3',
							tailBorderColor: '#1b0d0e',
							tailThirdColor: '#4a3607',
							tailFourthColor: '#404e50'
						}
					},
					demon: {
						tailFirstColor: '#2f5052',
						tailSecondColor: '#518a8c',
						tailBorderColor: '#000000',
						tailThirdColor: '#5e898b',
						tailFourthColor: '#19292b'
					}
				}
			},
			1: {
				normal: {
					baby: {
						tailFirstColor: '#f5d885',
						tailSecondColor: '#ffffbd',
						tailBorderColor: '#95432b',
						tailThirdColor: '#f7db8a',
						tailFourthColor: '#e0b461'
					},
					adult: {
						tailFirstColor: '#e0c577',
						tailSecondColor: '#fdf7aa',
						tailBorderColor: '#802608',
						tailThirdColor: '#e4cb81',
						tailFourthColor: '#cda355'
					}
				},
				demon: {
					tailFirstColor: '#d1b158',
					tailSecondColor: '#f3ec94',
					tailBorderColor: '#8d4300',
					tailThirdColor: '#d1b158',
					tailFourthColor: '#ba8a32'
				},
				special: {
					normal: {
						baby: {
							tailFirstColor: '#ae5004',
							tailSecondColor: '#ce8732',
							tailBorderColor: '#710a05',
							tailThirdColor: '#bb6003',
							tailFourthColor: '#992c05'
						},
						adult: {
							tailFirstColor: '#9e4500',
							tailSecondColor: '#bc7929',
							tailBorderColor: '#610000',
							tailThirdColor: '#9e4700',
							tailFourthColor: '#8a2300'
						}
					},
					demon: {
						tailFirstColor: '#841f00',
						tailSecondColor: '#a65a00',
						tailBorderColor: '#3e0000',
						tailThirdColor: '#897249',
						tailFourthColor: '#6d0000'
					}
				}
			},
			2: {
				normal: {
					baby: {
						tailFirstColor: '#f68302',
						tailSecondColor: '#ffba22',
						tailBorderColor: '#96342a',
						tailThirdColor: '#feb954',
						tailFourthColor: '#e25f03'
					},
					adult: {
						tailFirstColor: '#f49404',
						tailSecondColor: '#ffa919',
						tailBorderColor: '#800701',
						tailThirdColor: '#e8a648',
						tailFourthColor: '#ce5300'
					}
				},
				demon: {
					tailFirstColor: '#d35600',
					tailSecondColor: '#f59100',
					tailBorderColor: '#8b2b24',
					tailThirdColor: '#d35600',
					tailFourthColor: '#bc2f00'
				}
			},
			3: {
				normal: {
					baby: {
						tailFirstColor: '#860606',
						tailSecondColor: '#a73808',
						tailBorderColor: '#4b0605',
						tailThirdColor: '#a0300e',
						tailFourthColor: '#860606'
					},
					adult: {
						tailFirstColor: '#780000',
						tailSecondColor: '#972e02',
						tailBorderColor: '#3f0000',
						tailThirdColor: '#b36b49',
						tailFourthColor: '#640000'
					}
				},
				demon: {
					tailFirstColor: '#590000',
					tailSecondColor: '#7c0500',
					tailBorderColor: '#120000',
					tailThirdColor: '#9c4c24',
					tailFourthColor: '#410000'
				}
			},
			4: {
				normal: {
					baby: {
						tailFirstColor: '#935f44',
						tailSecondColor: '#b2977b',
						tailBorderColor: '#5c0b0a',
						tailThirdColor: '#bf996c',
						tailFourthColor: '#834329'
					},
					adult: {
						tailFirstColor: '#84533a',
						tailSecondColor: '#9f846a',
						tailBorderColor: '#480000',
						tailThirdColor: '#b99666',
						tailFourthColor: '#703117'
					}
				},
				demon: {
					tailFirstColor: '#672f13',
					tailSecondColor: '#8a6b4e',
					tailBorderColor: '#220000',
					tailThirdColor: '#a37b45',
					tailFourthColor: '#500800'
				}
			},
			5: {
				normal: {
					baby: {
						tailFirstColor: '#f1a802',
						tailSecondColor: '#ffdf16',
						tailBorderColor: '#943a28',
						tailThirdColor: '#fbca52',
						tailFourthColor: '#d88302'
					},
					adult: {
						tailFirstColor: '#de9900',
						tailSecondColor: '#fccd0f',
						tailBorderColor: '#7e1602',
						tailThirdColor: '#de9e09',
						tailFourthColor: '#ca7600'
					}
				},
				demon: {
					tailFirstColor: '#cd7e00',
					tailSecondColor: '#efba00',
					tailBorderColor: '#891000',
					tailThirdColor: '#d6a221',
					tailFourthColor: '#b65700'
				}
			},
			6: {
				normal: {
					baby: {
						tailFirstColor: '#3a3d54',
						tailSecondColor: '#59748c',
						tailBorderColor: '#19161b',
						tailThirdColor: '#9f967e',
						tailFourthColor: '#271d34'
					},
					adult: {
						tailFirstColor: '#303349',
						tailSecondColor: '#4e677e',
						tailBorderColor: '#1a0000',
						tailThirdColor: '#8f856e',
						tailFourthColor: '#1c1027'
					}
				},
				demon: {
					tailFirstColor: '#070a23',
					tailSecondColor: '#26405a',
					tailBorderColor: '#000000',
					tailThirdColor: '#72674d',
					tailFourthColor: '#000000'
				}
			},
			7: {
				normal: {
					baby: {
						tailFirstColor: '#d45407',
						tailSecondColor: '#f58b3e',
						tailBorderColor: '#8e0a06',
						tailThirdColor: '#e89347',
						tailFourthColor: '#b73904'
					},
					adult: {
						tailFirstColor: '#c24900',
						tailSecondColor: '#e17d34',
						tailBorderColor: '#860000',
						tailThirdColor: '#d9914a',
						tailFourthColor: '#af2700'
					}
				},
				demon: {
					tailFirstColor: '#ae2400',
					tailSecondColor: '#d15f0c',
					tailBorderColor: '#680000',
					tailThirdColor: '#c77624',
					tailFourthColor: '#970000'
				}
			},
			8: {
				normal: {
					baby: {
						tailFirstColor: '#ab3304',
						tailSecondColor: '#cc6a22',
						tailBorderColor: '#6d0804',
						tailThirdColor: '#ce7f45',
						tailFourthColor: '#960e05'
					},
					adult: {
						tailFirstColor: '#9b2a00',
						tailSecondColor: '#ba5e1a',
						tailBorderColor: '#600000',
						tailThirdColor: '#c5824a',
						tailFourthColor: '#870800'
					}
				},
				demon: {
					tailFirstColor: '#810000',
					tailSecondColor: '#a43c00',
					tailBorderColor: '#3b0000',
					tailThirdColor: '#b06323',
					tailFourthColor: '#6a0000'
				}
			}
		},
		hair: {
			0: {
				normal: {
					baby: {
						hairColor: '#fbd081'
					},
					adult: {
						hairColor: '#e7c275'
					}
				},
				demon: {
					hairColor: '#d6aa54'
				},
				special: {
					normal: {
						baby: {
							hairColor: '#39b1b8'
						},
						adult: {
							hairColor: '#30a3a8'
						}
					},
					demon: {
						hairColor: '#078d94'
					}
				}
			},
			1: {
				normal: {
					baby: {
						hairColor: '#fab047'
					},
					adult: {
						hairColor: '#e7a33e'
					}
				},
				demon: {
					hairColor: '#da8b17'
				},
				special: {
					normal: {
						baby: {
							hairColor: '#8a8603'
						},
						adult: {
							hairColor: '#7b7700'
						}
					},
					demon: {
						hairColor: '#5c5800'
					}
				}
			},
			2: {
				normal: {
					baby: {
						hairColor: '#f99312'
					},
					adult: {
						hairColor: '#e9870c'
					}
				},
				demon: {
					hairColor: '#dd6f00'
				}
			},
			3: {
				normal: {
					baby: {
						hairColor: '#e6dd7d'
					},
					adult: {
						hairColor: '#d4cc71'
					}
				},
				demon: {
					hairColor: '#c8c259'
				}
			},
			4: {
				normal: {
					baby: {
						hairColor: '#c7dd56'
					},
					adult: {
						hairColor: '#b5c84b'
					}
				},
				demon: {
					hairColor: '#9cb425'
				}
			},
			5: {
				normal: {
					baby: {
						hairColor: '#d1cb94'
					},
					adult: {
						hairColor: '#bfb985'
					}
				},
				demon: {
					hairColor: '#a8a166'
				}
			},
			6: {
				normal: {
					baby: {
						hairColor: '#94ae92'
					},
					adult: {
						hairColor: '#86a187'
					}
				},
				demon: {
					hairColor: '#6a8a6e'
				}
			},
			7: {
				normal: {
					baby: {
						hairColor: '#898d7b'
					},
					adult: {
						hairColor: '#7d8373'
					}
				},
				demon: {
					hairColor: '#5c614e'
				}
			},
			8: {
				normal: {
					baby: {
						hairColor: '#dd6f22'
					},
					adult: {
						hairColor: '#c96119'
					}
				},
				demon: {
					hairColor: '#b54000'
				}
			},
			9: {
				normal: {
					baby: {
						hairColor: '#b85514'
					},
					adult: {
						hairColor: '#a6490c'
					}
				},
				demon: {
					hairColor: '#8e2500'
				}
			},
			A: {
				normal: {
					baby: {
						hairColor: '#d21911'
					},
					adult: {
						hairColor: '#bf100a'
					}
				},
				demon: {
					hairColor: '#ab0000'
				}
			}
		}
	}
};

interface Pigmou {
	head: {
		position: {
			[headNumber: string]: {
				top: number;
				left: number;
			};
		};
		special: {
			top: number;
			left: number;
		};
		injury: {
			minor: {
				[minorInjuryNumber: string]: {
					imgNumber: number;
					top: number;
					left: number;
					rotation?: number;
				};
			};
		};
		eye: {
			[eyeNumber: string]: {
				eyebrow: {
					imgNumber: number;
					top: number;
					left: number;
				};
				sclera: {
					normal: {
						imgNumber: number;
						top: number;
						left: number;
					};
					demon: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				pupil?: {
					imgNumber?: number;
					baby?: {
						top: number;
						left: number;
					};
					adult?: {
						top: number;
						left: number;
					};
					dependentImage0?: {
						normal?: {
							imgNumber: number;
							top: number;
							left: number;
						};
						demon?: {
							imgNumber: number;
							top: number;
							left: number;
						};
					};
				};
				injury: {
					heavy: {
						[heavyInjuryNumber: string]: {
							imgNumber: number;
							top: number;
							left: number;
							zIndex?: number;
							rotation?: number;
						};
					};
				};
			};
		};
		mouth: {
			[mouthNumber: string]: {
				imgNumber: number;
				top: number;
				left: number;
				dependentImage0?: {
					normal?: {
						imgNumber: number;
						imgSize?: number;
						imgZIndex: number;
						top: number;
						left: number;
					};
					demon?: {
						imgNumber: number;
						imgSize?: number;
						imgZIndex: number;
						top: number;
						left: number;
					};
				};
				dependentImage1?: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
		};
	};
	body: {
		default: {
			top: number;
			left: number;
		};
		leg: {
			front: {
				left: {
					baby: {
						imgSize: number;
						position: {
							top: number;
							left: number;
						};
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
					adult: {
						imgSize: number;
						position: {
							top: number;
							left: number;
						};
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
					demon: {
						imgSize: number;
						position: {
							top: number;
							left: number;
						};
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
				};
				right: {
					baby: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						injury: {
							minor: {
								top: number;
								left: number;
							};
							heavy: {
								[heavyInjuryNumber: string]: {
									top: number;
									left: number;
								};
							};
						};
						special: {
							top: number;
							left: number;
						};
					};
					adult: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						injury: {
							minor: {
								top: number;
								left: number;
							};
							heavy: {
								[heavyInjuryNumber: string]: {
									top: number;
									left: number;
								};
							};
						};
						special: {
							top: number;
							left: number;
						};
					};
					demon: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						injury: {
							minor: {
								top: number;
								left: number;
							};
							heavy: {
								[heavyInjuryNumber: string]: {
									top: number;
									left: number;
								};
							};
						};
						special: {
							top: number;
							left: number;
						};
					};
				};
			};
			back: {
				right: {
					baby: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
					adult: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
					demon: {
						top: number;
						left: number;
						imgSize: number;
						nail: {
							top: number;
							left: number;
						};
						fur: {
							top: number;
							left: number;
						};
						special: {
							top: number;
							left: number;
						};
					};
				};
			};
		};
		chest: {
			baby: {
				imgSize: number;
				top: number;
				left: number;
				special: {
					top: number;
					left: number;
				};
			};
			adult: {
				imgSize: number;
				top: number;
				left: number;
				special: {
					top: number;
					left: number;
				};
			};
			demon: {
				imgSize: number;
				top: number;
				left: number;
				special: {
					top: number;
					left: number;
				};
			};
		};
	};
	tail: {
		defaultPosition: {
			top: number;
			left: number;
		};
		tail: {
			[tailNumber: string]: {
				imgNumber?: number;
				baby: {
					top: number;
					left: number;
					injury?: {
						heavy: {
							top: number;
							left: number;
							size: number;
						};
					};
				};
				adult: {
					top: number;
					left: number;
					injury?: {
						heavy: {
							top: number;
							left: number;
							size: number;
						};
					};
				};
				demon: {
					top: number;
					left: number;
					injury?: {
						heavy: {
							top: number;
							left: number;
							size: number;
						};
					};
				};
				dependentImage0?: {
					imgNumber: number;
					baby: {
						top: number;
						left: number;
					};
					adult: {
						top: number;
						left: number;
					};
					demon: {
						top: number;
						left: number;
					};
				};
			};
		};
	};
	hair: {
		[hairNumber: string]: {
			image1?: {
				imgNumber?: number;
				top?: number;
				left?: number;
				zIndex?: number;
				baby?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex?: number;
				};
				adult?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex?: number;
				};
				demon?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex?: number;
				};
			};
			image2?: {
				imgNumber?: number;
				top?: number;
				left?: number;
				zIndex?: number;
				baby?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex: number;
				};
				adult?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex: number;
				};
				demon?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex: number;
				};
			};
			image3?: {
				imgNumber?: number;
				top?: number;
				left?: number;
				zIndex?: number;
				baby?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				adult?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				demon?: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
			image4?: {
				imgNumber?: number;
				top?: number;
				left?: number;
				zIndex?: number;
				baby?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				adult?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				demon?: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
		};
	};
	color: {
		body: {
			[colorNumber: string]: {
				normal: {
					baby: {
						mainFirstColor: string;
						mainSecondColor: string;
						mainThirdColor: string;
						mainFourthColor: string;
						mainFifthColor: string;
						brighterSpecialColor: string;
						darkerSpecialColor: string;
					};
					adult: {
						mainFirstColor: string;
						mainSecondColor: string;
						mainThirdColor: string;
						mainFourthColor: string;
						mainFifthColor: string;
						brighterSpecialColor: string;
						darkerSpecialColor: string;
					};
				};
				demon: {
					mainFirstColor: string;
					mainSecondColor: string;
					mainThirdColor: string;
					mainFourthColor: string;
					mainFifthColor: string;
					brighterSpecialColor: string;
					darkerSpecialColor: string;
				};
				special?: {
					normal: {
						baby: {
							mainFirstColor: string;
							mainSecondColor: string;
							mainThirdColor: string;
							mainFourthColor: string;
							mainFifthColor: string;
							brighterSpecialColor: string;
							darkerSpecialColor: string;
						};
						adult: {
							mainFirstColor: string;
							mainSecondColor: string;
							mainThirdColor: string;
							mainFourthColor: string;
							mainFifthColor: string;
							brighterSpecialColor: string;
							darkerSpecialColor: string;
						};
					};
					demon: {
						mainFirstColor: string;
						mainSecondColor: string;
						mainThirdColor: string;
						mainFourthColor: string;
						mainFifthColor: string;
						brighterSpecialColor: string;
						darkerSpecialColor: string;
					};
				};
			};
		};
		tail: {
			[colorNumber: string]: {
				normal: {
					baby: {
						tailFirstColor: string;
						tailSecondColor: string;
						tailBorderColor: string;
						tailThirdColor: string;
						tailFourthColor: string;
					};
					adult: {
						tailFirstColor: string;
						tailSecondColor: string;
						tailBorderColor: string;
						tailThirdColor: string;
						tailFourthColor: string;
					};
				};
				demon: {
					tailFirstColor: string;
					tailSecondColor: string;
					tailBorderColor: string;
					tailThirdColor: string;
					tailFourthColor: string;
				};
				special?: {
					normal: {
						baby: {
							tailFirstColor: string;
							tailSecondColor: string;
							tailBorderColor: string;
							tailThirdColor: string;
							tailFourthColor: string;
						};
						adult: {
							tailFirstColor: string;
							tailSecondColor: string;
							tailBorderColor: string;
							tailThirdColor: string;
							tailFourthColor: string;
						};
					};
					demon: {
						tailFirstColor: string;
						tailSecondColor: string;
						tailBorderColor: string;
						tailThirdColor: string;
						tailFourthColor: string;
					};
				};
			};
		};
		hair: {
			[colorNumber: string]: {
				normal: {
					baby: {
						hairColor: string;
					};
					adult: {
						hairColor: string;
					};
				};
				demon: {
					hairColor: string;
				};
				special?: {
					normal: {
						baby: {
							hairColor: string;
						};
						adult: {
							hairColor: string;
						};
					};
					demon: {
						hairColor: string;
					};
				};
			};
		};
	};
}
