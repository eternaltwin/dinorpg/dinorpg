export const developers = [
	'Jérémy "Biosha" Guilbert',
	'Lucas "Jolu" Lesven',
	'Sylvain "Jahaa" Hunault',
	'Franck "Zen" Demoute',
	'Thibault "Gerardufoin" Duval',
	'Matthieu "Matthieu8360" Bonjour',
	'Augustin "Augustin" Janvier'
];

export const helpers = [
	' Simpkin',
	' PiouPiou',
	' ThePiouz',
	' Eliezer',
	' Valedres',
	' Stick-Man Smith',
	' A7',
	' Sininchi',
	' Ash'
];
